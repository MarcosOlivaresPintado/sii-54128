#include <vector>
#include "Plano.h"

#include "Esfera.h"
#include "Raqueta.h"
#include "DatosMemCompartida.h"

class CMundoCliente
{
public:
	void Init();
	CMundoCliente();
	virtual ~CMundoCliente();

	void InitGL();
	void OnKeyboardDown(unsigned char key, int x, int y);
	void OnTimer(int value);
	void OnDraw();

	std::vector<Esfera*> esferas;
	Esfera esfera;
	std::vector<Plano> paredes;
	Plano fondo_izq;
	Plano fondo_dcho;
	Raqueta jugador1;
	Raqueta jugador2;
	DatosMemCompartida datosCompartidos; //atributo de datos memoria
	DatosMemCompartida *pdatosCompartidos; //puntero a datos memoria

	int fifo_servidor_cliente;
	int fifo_cliente_servidor;

	int puntos1;
	int puntos2;

};
