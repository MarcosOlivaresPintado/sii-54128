#include <vector>
#include "Plano.h"

#include "Esfera.h"
#include "Raqueta.h"

class CMundoServidor
{
public:
	void Init();
	CMundoServidor();
	virtual ~CMundoServidor();

	void InitGL();
	void OnKeyboardDown(unsigned char key, int x, int y);
	void OnTimer(int value);
	void OnDraw();
	void RecibeComandosJugador();

	std::vector<Esfera*> esferas;
	Esfera esfera;
	std::vector<Plano> paredes;
	Plano fondo_izq;
	Plano fondo_dcho;
	Raqueta jugador1;
	Raqueta jugador2;
	int fd;
	int fifo_servidor_cliente;
	int fifo_cliente_servidor;

	pthread_t thid1;
	pthread_attr_t atrib;

	int puntos1;
	int puntos2;
};
