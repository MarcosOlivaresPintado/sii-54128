#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>
#include <string>
#include <iostream>
#include "Puntuaciones.h"

int main (void){
	int fd;
	Puntuaciones puntos;

	if (mkfifo("/tmp/FIFO_LOGGER", 0600) < 0){
		perror("No puede crearse el FIFO");
		return 1;
	}
	
	if ((fd=open("/tmp/FIFO_LOGGER", O_RDONLY)) < 0){
		perror("No puede abrirse el FIFO");
		return 1;
	}

	while (read(fd, &puntos, sizeof(puntos)) == sizeof(puntos)){
		if (puntos.lastWinner == 1){
			printf("Jugador 1 marca 1 punto, lleva un total de %d puntos.\n", puntos.jugador1);
		}
		else if (puntos.lastWinner == 2){
			printf("Jugador 2 marca 1 punto, lleva un total de %d puntos.\n", puntos.jugador2);

		}
	}

	close(fd);
	unlink("FIFO");
	return 0;
}
