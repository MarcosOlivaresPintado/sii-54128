#include <sys/types.h>
#include <sys/stat.h>
#include <sys/mman.h>
#include <fcntl.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <fstream>
#include "DatosMemCompartida.h"

int main(){
	int fd;
	DatosMemCompartida *pdatosCompartidos;
	fd = open("/tmp/datosCompartidos", O_RDWR);
	if (fd < 0) {
		perror("Error en la apertura del fichero");
		exit(1);
	}
	pdatosCompartidos=static_cast<DatosMemCompartida*>(mmap(0, sizeof(DatosMemCompartida), PROT_READ|PROT_WRITE, MAP_SHARED, fd, 0));
	if (pdatosCompartidos==MAP_FAILED){
		perror("Error en la proyeccion del fichero");
		close(fd);
		return 1;
	}
	close(fd);

while(pdatosCompartidos!=NULL){
	if(pdatosCompartidos->esfera.centro.y < pdatosCompartidos->raqueta1.getCentro().y){
		pdatosCompartidos->accion1=-1;
	}
	else if (pdatosCompartidos->esfera.centro.y == pdatosCompartidos->raqueta1.getCentro().y){
		pdatosCompartidos->accion1=0;
	}
	else if (pdatosCompartidos->esfera.centro.y > pdatosCompartidos->raqueta1.getCentro().y){
		pdatosCompartidos->accion1=1;
	}
//	if(pdatosCompartidos->esfera.centro.y < pdatosCompartidos->raqueta2.getCentro().y){
//                pdatosCompartidos->accion2=-1;
//        }
//        else if(pdatosCompartidos->esfera.centro.y==pdatosCompartidos->raqueta2.getCentro().y){
//                pdatosCompartidos->accion2=0;
//        }
//      else if(pdatosCompartidos->esfera.centro.y>pdatosCompartidos->raqueta2.getCentro().y){
//                pdatosCompartidos->accion2=1;
//        }
	usleep(25000);
}
	unlink("/tmp/datosCompartidos");
	return 1;
}
