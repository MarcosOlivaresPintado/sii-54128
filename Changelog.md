# Changelog

## [1.4] - 2020-12-14

### Se añadió:
- Ahora se pueden generar dos pantallas en las que pasa exactamente lo
mismo, Cliente y Servidor.

## [1.3] - 2020-11-19

### Se añadió:
- Ahora se crea una esfera cada vez que se pulsa 'b' sin límite.
- El logger va actualizando por pantalla el marcador.
- Se le da movimiento a la raqueta1 con el bot.

## [1.2] - 2020-11-16

### Se añadió:
- Se crea una esfera cada vez que se consigue un punto hasta 3 esferas.

## [1.1] - 2020-10-29

### Se añadió:
- Movimiento a las raquetas y esferas.
- La esfera reduce su tamaño según avance el tiempo.

## [1.0] - 2020-10-28

### Se añadió
- Changelog.
